#include <hardware/flash.h>
#include "tusb.h"
#include "class/audio/audio.h"
#include "usb_descriptors.h"

/* Device Descriptor */
static tusb_desc_device_t const desc_device = {
	.bLength		= sizeof(tusb_desc_device_t),
	.bDescriptorType	= TUSB_DESC_DEVICE,
	.bcdUSB			= 0x0200,
	.bDeviceClass		= TUSB_CLASS_MISC,
	.bDeviceSubClass	= MISC_SUBCLASS_COMMON,
	.bDeviceProtocol	= MISC_PROTOCOL_IAD,
	.bMaxPacketSize0	= CFG_TUD_ENDPOINT0_SIZE,

	.idVendor		= 0x1d50,
	.idProduct		= 0x6187,
	.bcdDevice		= 0x0100,

	.iManufacturer		= 1,
	.iProduct		= 2,
	.iSerialNumber		= 3,

	.bNumConfigurations	= 1
};

/* Invoked when received GET DEVICE DESCRIPTOR */
uint8_t const* tud_descriptor_device_cb(void)
{
	return (uint8_t const *) &desc_device;
}


/* Configuration Descriptor */
#define CONFIG_TOTAL_LEN (TUD_CONFIG_DESC_LEN + TUD_CDC_DESC_LEN * CFG_TUD_CDC)
static uint8_t const desc_config[] = {
	// Config number, interface count, string index, total length, attribute, power in mA
	TUD_CONFIG_DESCRIPTOR(1, ITF_NUM_TOTAL, 0, CONFIG_TOTAL_LEN, TUSB_DESC_CONFIG_ATT_REMOTE_WAKEUP, 100),

	TUD_CDC_DESCRIPTOR(0, 4, 0x81, 8, 0x01, 0x82, 64),
	TUD_CDC_DESCRIPTOR(2, 5, 0x83, 8, 0x03, 0x84, 64),
};

/* Invoked when received GET CONFIGURATION DESCRIPTOR */
uint8_t const* tud_descriptor_configuration_cb(uint8_t index)
{
	// This example use the same configuration for both high and full speed mode
	return desc_config;
}

/* String Descriptors */
#define USBD_STR_SERIAL_LEN 17
static char usbd_serial[USBD_STR_SERIAL_LEN] = "000000000000";
char const  *string_desc_arr[] = {
	(const char []) { 0x09, 0x04},	/* 0: supported language == English */
	"sysmocom - s.f.m.c. GmbH",	/* 1: Manufacturer */
	"osmo-gtm900",			/* 2: Product */
	usbd_serial,			/* 3: Serial, should use chip ID */
	"UART1",
	"UART2",
};

static uint16_t _desc_str[32];

// Invoked when received GET STRING DESCRIPTOR request
// Application return pointer to descriptor, whose contents must exist long enough for transfer to complete
uint16_t const* tud_descriptor_string_cb(uint8_t index, uint16_t langid)
{
	(void) langid;

	uint8_t chr_count;

	if (index == 0) {
		memcpy(&_desc_str[1], string_desc_arr[0], 2);
		chr_count = 1;
	} else {
		// Convert ASCII string into UTF-16
		if (!(index < sizeof(string_desc_arr)/sizeof(string_desc_arr[0])))
			return NULL;

    		const char* str = string_desc_arr[index];

		// Cap at max char
		chr_count = (uint8_t) strlen(str);
		if (chr_count > 31)
			chr_count = 31;

		for (uint8_t i = 0; i < chr_count; i++) {
			_desc_str[1+i] = str[i];
		}
	}

	// first byte is length (including header), second byte is string type
	_desc_str[0] = (uint16_t) ((TUSB_DESC_STRING << 8 ) | (2*chr_count + 2));

	return _desc_str;
}

void usbd_serial_init(void)
{
	uint8_t id[8];

	flash_get_unique_id(id);

	snprintf(usbd_serial, USBD_STR_SERIAL_LEN, "%02X%02X%02X%02X%02X%02X%02X%02X",
		 id[0], id[1], id[2], id[3], id[4], id[5], id[6], id[7]);
}
