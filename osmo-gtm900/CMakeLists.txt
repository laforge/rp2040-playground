add_executable(osmo-gtm900)

target_compile_options(osmo-gtm900 PRIVATE -DPARAM_ASSERTIONS_ENABLE_ALL=1)

target_sources(osmo-gtm900 PRIVATE osmo_gtm900.c
                                   usb_descriptors.c)

# Make sure TinyUSB can find tusb_config.h
target_include_directories(osmo-gtm900 PUBLIC
        ${CMAKE_CURRENT_LIST_DIR})

target_link_libraries(osmo-gtm900 PRIVATE
        pico_stdlib
        pico_multicore
        hardware_flash
        tinyusb_device
        tinyusb_board
        cmsis_core
        )

pico_enable_stdio_usb(osmo-gtm900 1)

pico_add_extra_outputs(osmo-gtm900)
