#pragma once

/* Interface numbers */
enum {
	ITF_NUM_UART1 = 0,
	ITF_NUM_UART1_ASSOC,
	ITF_NUM_UART2,
	ITF_NUM_UART2_ASSOC,
	ITF_NUM_TOTAL
};

void usbd_serial_init(void);
